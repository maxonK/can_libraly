#include <CAN_Library.h>

CANint CAN0(/*CS*/10,/*INT*/9);
void setup()
{
  Serial.begin(115200);
  if(CAN0.CANbegin(CAN_8MHz_1000KBPS,3) == CAN_OK) Serial.print("can init ok!!\r\n");
  else Serial.print("Can init fail!!\r\n");
}

void loop()
{
  CAN0.CANsetId(0x00);
  CAN0.CANsetData(0,1023);
  CAN0.CANsetData(1,256);
  CAN0.CANsetData(2,39);
  CAN0.CANsetData(3,3939);
  CAN0.CANsetData(4,555);
  CAN0.CANsetData(5,666);
  CAN0.CANsetData(6,14);
  CAN0.CANsend();
}