#include <CAN_Library.h>

CANint CAN0(/*CS*/10,/*INT*/9);
void setup()
{
  Serial.begin(115200);
  Serial.println("MCP2515 Library Receive Example...");

  CAN0.CANbegin(CAN_8MHz_1000KBPS,3);
}

void loop()
{
 
    if(CAN0.CANavailable())                         // If pin 2 is low, read receive buffer
    { 
      CAN0.CANload();                
      Serial.print("ID: ");
      Serial.print(CAN0.CANgetId(), BIN);
      
      Serial.print("  Data0: ");
      Serial.print(CAN0.CANgetData(0));
      Serial.print("  Data1: ");
      Serial.print(CAN0.CANgetData(1));
      Serial.print("  Data2: ");
      Serial.print(CAN0.CANgetData(2));
      Serial.print("  Data3: ");
      Serial.print(CAN0.CANgetData(3));
      Serial.print("  Data4: ");
      Serial.print(CAN0.CANgetData(4));
      Serial.print("  Data5: ");
      Serial.print(CAN0.CANgetData(5));
      Serial.print("  Data6: ");
      Serial.print(CAN0.CANgetData(6));
      Serial.println();
    }
}