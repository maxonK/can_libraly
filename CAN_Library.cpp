 #include "arduino.h"
 #include <mcp_can_8MHz.h>
 #include <SPI.h>
 #include "CAN_Library.h"
  //コンストラクタ(初期化関数。インスタンス生成のときに実行される
  CANint :: CANint(int iCSPin , int iINTPin): CAN (iCSPin)
  {
    len = 0;
    //CSピン番号セット
    CSPin = iCSPin;
    //INTピン番号セット
    INTPin = iINTPin;
    //INTピンセット
    pinMode(INTPin,INPUT);
	//共用体変数初期化
	rxUni.byteStr[0] = 0;
	rxUni.byteStr[1] = 0;
	rxUni.byteStr[2] = 0;
	rxUni.byteStr[3] = 0;
	rxUni.byteStr[4] = 0;
	rxUni.byteStr[5] = 0;
	rxUni.byteStr[6] = 0;
	rxUni.byteStr[7] = 0;
    rxId = 0;
	txUni.byteStr[0] = 0;
	txUni.byteStr[1] = 0;
	txUni.byteStr[2] = 0;
	txUni.byteStr[3] = 0;
	txUni.byteStr[4] = 0;
	txUni.byteStr[5] = 0;
	txUni.byteStr[6] = 0;
	txUni.byteStr[7] = 0;
    txId = 0;
    //データ長
    byteLength = 8;
	//前回send時刻
	lastSendMicros = 0;
	//最小送信間隔デフォルト設定
	minSendIntervalMicros = 200;
	
  }
  bool CANint :: CANbegin(int bps,int nodeQty){
	  double bpsValue = 0.0;
	  switch (bps){
		case CAN_5KBPS:
			bpsValue = CAN_5KBPSValue;
			break;
		case CAN_10KBPS:
			bpsValue = CAN_10KBPSValue;
			break;
		case CAN_20KBPS:
			bpsValue = CAN_20KBPSValue;
			break;
		case CAN_31K25BPS:
			bpsValue = CAN_31K25BPSValue;
			break;
		case CAN_40KBPS:
			bpsValue = CAN_40KBPSValue;
			break;
		case CAN_50KBPS:
			bpsValue = CAN_50KBPSValue;
			break;
		case CAN_80KBPS:
			bpsValue = CAN_80KBPSValue;
			break;
		case CAN_100KBPS:
			bpsValue = CAN_100KBPSValue;
			break;
		case CAN_125KBPS:
			bpsValue = CAN_125KBPSValue;
			break;
		case CAN_200KBPS:
			bpsValue = CAN_200KBPSValue;
			break;
		case CAN_250KBPS:
			bpsValue = CAN_250KBPSValue;
			break;
		case CAN_500KBPS:
			bpsValue = CAN_500KBPSValue;
			break;
		case CAN_1000KBPS:
			bpsValue = CAN_1000KBPSValue;
			break;
		case CAN_8MHz_1000KBPS:
			bpsValue = CAN_8MHz_1000KBPSValue;
			break;
	  default :
			bpsValue = CAN_5KBPSValue;
			break;
	  }
	  minSendIntervalMicros = (unsigned long)(((120.0 * (1.0 / bpsValue)) * 1000000.0)*(double)nodeQty);
    return CAN.begin(bps);                       // init can bus : baudrate = 500k 
  }
  bool CANint :: CANavailable(void){
    int pinState = false;
    pinState = !digitalRead(INTPin);
    return pinState;
  }
  void CANint :: CANload(void){
    CAN.readMsgBuf(&len, rxUni.byteStr); 
    rxId = CAN.getCanId();
  }
  long unsigned int CANint :: CANgetId(void){
    return rxId;
  }
  int CANint :: CANgetData(int dataNo){
    switch(dataNo){
      case 0:
		  return rxUni.int10str.bit10Int0;
      case 1:
		  return rxUni.int10str.bit10Int1;
      case 2:
		  return rxUni.int10str.bit10Int2;
      case 3:
		  return rxUni.int10str.bit10Int3;
	  case 4:
		  return rxUni.int10str.bit10Int4;
	  case 5:
		  return rxUni.int10str.bit10Int5;
	  case 6:
		  return rxUni.int10str.bit4Int0;
      default:
        return 0;
    }
  }
  void CANint :: CANsetId(long unsigned int itxId){
    txId = itxId;
  }
  void CANint :: CANsetData(int dataNo, int data){
    switch(dataNo){
      case 0:
		  txUni.int10str.bit10Int0 = constrain(data,0,1023);
        break;
      case 1:
		  txUni.int10str.bit10Int1 = constrain(data, 0, 1023);
        break;
      case 2:
		  txUni.int10str.bit10Int2 = constrain(data, 0, 1023);
        break;
      case 3:
		  txUni.int10str.bit10Int3 = constrain(data, 0, 1023);
        break;
	  case 4:
		  txUni.int10str.bit10Int4 = constrain(data, 0, 1023);
		  break;
	  case 5:
		  txUni.int10str.bit10Int5 = constrain(data, 0, 1023);
		  break;
	  case 6:
		  txUni.int10str.bit4Int0 = constrain(data, 0, 14);
		  break;
      default:
        break;
    }
  }
  void CANint :: CANsend(void){
	  if ((micros() - lastSendMicros) > minSendIntervalMicros){
		  CAN.sendMsgBuf(txId, 0, byteLength, txUni.byteStr);
		  lastSendMicros = micros();
	  }
    return;
  }