 #ifndef CAN_Library_h
 #define CAN_Library_h

 #include <mcp_can_8MHz.h>
 #include <SPI.h>
 #include "arduino.h"

#define CAN_5KBPSValue    5000.0
#define CAN_10KBPSValue   10000.0
#define CAN_20KBPSValue   20000.0
#define CAN_31K25BPSValue 31250.0
#define CAN_40KBPSValue   40000.0
#define CAN_50KBPSValue   50000.0
#define CAN_80KBPSValue   80000.0
#define CAN_100KBPSValue  100000.0
#define CAN_125KBPSValue  125000.0
#define CAN_200KBPSValue  200000.0
#define CAN_250KBPSValue  250000.0
#define CAN_500KBPSValue  500000.0
#define CAN_1000KBPSValue 1000000.0
#define CAN_8MHz_1000KBPSValue 1000000.0

class CANint{
  public://public
  CANint(int ,int);
  bool CANbegin(int,int);
  bool CANavailable(void);
  void CANload(void);
  void CANsend(void);
  long unsigned int CANgetId(void);
  void CANsetId(long unsigned int);
  int CANgetData(int);
  void CANsetData(int,int);
  
  private://private
  unsigned char len;
  int CSPin;
  int INTPin;
  long unsigned int rxId;
  long unsigned int txId;
  int byteLength;
  unsigned long lastSendMicros;
  unsigned long minSendIntervalMicros;
  //���p�̗p�̍\����
  typedef struct {
	  unsigned int bit4Int0 : 4;
	  unsigned int bit10Int0 : 10;
	  unsigned int bit10Int1 : 10;
	  unsigned int bit10Int2 : 10;
	  unsigned int bit10Int3 : 10;
	  unsigned int bit10Int4 : 10;
	  unsigned int bit10Int5 : 10;
  }int10;
  //byte�^�z���int�^�z��̋��p��
  union str{
  byte  byteStr[8];//byte�^�z��
  int10 int10str;//10bit��int�^�\����
  /*int   intStr[8/2];//int�^�z��*/
  /*int[0]LowByte int[0]HighByte
    byte[0]       byte[1]]*/
  }rxUni,txUni;
  MCP_CAN CAN;
};

#endif